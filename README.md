Jira Automation
====================

* The project is only equipped to handle one domain, http://pratik.atlassian.net.
* assertions are based on my knowledge of Jira.
* Two types of testing have been created for the automated tests, suite based and individual script based.
* To spare you from having to install virtualenvwrapper and other various steps that are typically automated, please simply install pip run requirements.txt **(see steps below)**
* I had to use a few sleeps, which I generally reserve for only extreme tempermental scenarios, but there should be a workaround that uses wait or other methods in python SST.
However, I didn't feel it was necessary to spend too much time on this. 
* virtualdisplay can be used for either types of testing below so tests can silently in the background without browser launches.  This is easily setup on Ubuntu but requires tweaking on mac, so I'm leaving it out, but its a great feature to run locally or on a remote environment. 


### Setup Steps ###

1. sudo easy_install pip
2. sudo mkdir -p /var/www/ ( or cd /var/www/)
3. cd /var/www/
4. sudo chown <username>:<username> -R /var/www/ && sudo chmod 755 -R /var/www/
5. git clone https://hookempk@bitbucket.org/hookempk/jira-automation.git
6. sudo pip install -r requirements.txt    
7. export PYTHONPATH=/var/www/jira-automation


## Two Types of Testing Provided ##

###Suite based testing###

Pros and Cons

Allows us to track the original issue created, edited and searched creating a breadcrumb.
Downside to this is if one test breaks it more than likely will fail remaining dependent testcases.


 
cd /var/www/jira-automation/web-tests/suite-issues

run: python -m unittest test_create_edit_search_issues
 

###Script based Testing###

Pros and Cons:

Compartmentalized testing and quickly write scripts.
Downside to this is relying on pre-existing and sometimes untampered with data.

 
cd /var/www/jira-automation/web-tests/sst-issues

run: sst-run