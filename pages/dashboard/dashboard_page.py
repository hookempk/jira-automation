__author__ = 'pratik'
from sst.actions import *
from sst.config import flags


class DashboardPage(object):

    def __init__(self):
        self.environment = flags[0]
        self.suffix = '/secure/Dashboard.jspa'
        self.url = 'https://%s.atlassian.net' % self.environment + self.suffix
        self.title = 'Automation - Jira'

    #Top Navigation elements

    @property
    def create_issue_button(self):
        element = get_element(id='create_link')
        return element

    @property
    def issues_dropdown(self):
        element = get_element(id='find_link')
        return element

    @property
    def get_issue_key_element(self):
        element = get_element(css_class='issue-created-key')
        return element


    @property
    def quick_search_input_field(self):
        element = get_element(id='quickSearchInput')
        return element