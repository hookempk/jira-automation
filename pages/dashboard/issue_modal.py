__author__ = 'pratik'
from sst.actions import *


class IssueModal(object):

    # Modals
    @property
    def create_issue_modal(self):
        element = get_element(id='create-issue-dialog')
        return element

    @property
    def edit_issue_modal(self):
        element = get_element(id='create-issue-dialog')
        return element

    # Modal Elements
    @property
    def project_dropdown(self):
        element = get_element(id='project-field')
        return element

    @property
    def issue_type_dropdown(self):
        element = get_element(id='issuetype-field')
        return element

    @property
    def priority_dropdown(self):
        element = get_element(id='priority-field')
        return element

    @property
    def assignee_dropdown(self):
        element = get_element(id='assignee_field_dropdown')
        return element
    @property
    def summary_field(self):
        element = get_element(id='summary')
        return element

    @property
    def description(self):
        element = get_element(id='description')
        return element

    @property
    def submit_issue_button(self):
        element = get_element(id='create-issue-submit')
        return element

    @property
    def update_issue_button(self):
        element = get_element(id='edit-issue-submit')
        return element


