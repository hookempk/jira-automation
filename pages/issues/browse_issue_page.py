__author__ = 'pratik'
from sst.actions import *


class IssuePage(object):

    def __init__(self):
        self.issue_id = ''
        self.url = 'https://pratik.atlassian.net/browse/%s' % self.issue_id

    @property
    def edit_issue_link(self):
        element = get_element(id='edit-issue')
        return element

    @property
    def issue_summary(self):
        element = get_element(id='summary-val')
        return element

    @property
    def comment_button(self):
        element = get_element(id='footer-comment-button')
        return element