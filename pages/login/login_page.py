__author__ = 'pratik'
from sst.actions import *
from sst.config import flags


class LoginPage(object):

    def __init__(self):
        self.environment = flags[0]
        self.url = 'https://%s.atlassian.net' % self.environment
        self.suffix = '/login?dest-url=%2Fsecure%2FDashboard.jspa'
        self.qualified_url = self.url + self.suffix
        self.title = 'Automation - Jira'

    @property
    def username(self):
        element = get_element(id='username')
        return element

    @property
    def password(self):
        element = get_element(id='password')
        return element

    @property
    def login_button(self):
        element = get_element(id='login')
        return element


