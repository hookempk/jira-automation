__author__ = 'pratik'

from sst.actions import *
from sst import config
config.flags.insert(0,'pratik')

from pages.login.login_page import LoginPage
from pages.dashboard.dashboard_page import DashboardPage

home_page = LoginPage()
dashboard_page = DashboardPage()


def login():
        go_to(home_page.url)
        assert_url(home_page.qualified_url)
        write_textfield(home_page.username, 'pratik')
        write_textfield(home_page.password, 'password1')
        click_button(home_page.login_button)
        assert_url(dashboard_page.url)


class SharedMethods(object):

    def login(self):
            go_to(home_page.url)
            assert_url(home_page.qualified_url)
            write_textfield(home_page.username, 'pratik')
            write_textfield(home_page.password, 'password1')
            click_button(home_page.login_button)
            assert_url(dashboard_page.url)