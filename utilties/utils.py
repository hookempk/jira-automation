import datetime

def get_timestamp():
    return str(datetime.datetime.now()).split('.')[0]

def get_key_from_url(url):
    key = url.rsplit('/',1)
    return key[1]

