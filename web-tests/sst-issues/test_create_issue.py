__author__ = 'pratik'
from sst.actions import *
from pages.shared.shared_methods import login
from utilties.utils import *
from pages.dashboard.issue_modal import IssueModal
from pages.dashboard.dashboard_page import DashboardPage
from pages.issues.browse_issue_page import IssuePage

dashboard_page = DashboardPage()
issue_modal = IssueModal()
issue_page = IssuePage()
time = get_timestamp()
issue_creation_summary = 'Test Issue Creation Summary: ' + time
issue_creation_description = 'Test Issue Creation Description: ' + time

login()

#assert user lands on the dashboard page
click_link(dashboard_page.create_issue_button)
sleep(5)

#Entering Modal.  Begin by asserting Modal id to ensure modal is present.
assert_element(id='create-issue-dialog')
write_textfield(issue_modal.summary_field, issue_creation_summary)
write_textfield(issue_modal.description, issue_creation_description)
click_button(issue_modal.submit_issue_button)

#modal closes. assert notification and assign global key variable the url from notification element.
wait_for(get_element, css_class='issue-created-key')
issue_key_url = get_link_url(dashboard_page.get_issue_key_element)

#assert user lands on issue page
go_to(issue_key_url)
wait_for(assert_element , id='footer-comment-button')
