__author__ = 'pratik'
from sst.actions import *
from pages.shared.shared_methods import login
from utilties.utils import *
from pages.dashboard.issue_modal import IssueModal
from pages.dashboard.dashboard_page import DashboardPage
from pages.issues.browse_issue_page import IssuePage

dashboard_page = DashboardPage()
issue_modal = IssueModal()
issue_page = IssuePage()
time = get_timestamp()


issue = 'https://pratik.atlassian.net/browse/AUT-13'
issue_edit_summary = "Test Issue Edit Summary: " + time
issue_edit_description = "Test Issue Edit Description: " + time

# login method handles verifying user landed on dashboard page
login()
go_to(issue)
click_link(issue_page.edit_issue_link)
sleep(5)

# get prexisting summary title
existing_summary = get_text(issue_page.issue_summary)

#Entering Edit modal.  Begin by asserting Edit Modal id to ensure modal is present.
assert_element(id='edit-issue-dialog')
write_textfield(issue_modal.summary_field, issue_edit_summary)
write_textfield(issue_modal.description, issue_edit_description)
click_element(issue_modal.update_issue_button)
sleep(5)
#assert summary values have changed.
assert_not_equal(existing_summary, issue_edit_summary)
