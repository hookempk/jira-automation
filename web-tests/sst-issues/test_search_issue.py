from sst.actions import *
from pages.shared.shared_methods import login
from pages.issues.browse_issue_page import IssuePage
from pages.dashboard.dashboard_page import DashboardPage

dashboard_page = DashboardPage()
issue_page = IssuePage()

issue_to_search = 'Sample by Hand creation'

login()
write_textfield(dashboard_page.quick_search_input_field,  issue_to_search)
simulate_keys(dashboard_page.quick_search_input_field, 'ENTER')

# assert that summary of issue page is what we searched for
assert_text(issue_page.issue_summary, issue_to_search)