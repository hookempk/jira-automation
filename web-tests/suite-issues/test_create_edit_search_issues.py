__author__ = 'pratik'

"""
Description:

A Jira user can create, edit and search for an existing issue.

a. create issue
b. edit same issue
c. search edited issue

"""
from sst.actions import *
from sst import cases

from pages.shared.shared_methods import login
from utilties.utils import *
from pages.dashboard.issue_modal import IssueModal
from pages.dashboard.dashboard_page import DashboardPage
from pages.issues.browse_issue_page import IssuePage

dashboard_page = DashboardPage()
issue_modal = IssueModal()
issue_page = IssuePage()


class JiraIssues(cases.SSTTestCase):

    def __init__(self, *args, **kwargs):
        super(JiraIssues, self).__init__(*args, **kwargs)
        self.wait_poll = 1.5
        self.time = get_timestamp()
        self.issue_creation_summary = 'Test Issue Creation Summary: ' + self.time
        self.issue_creation_description = 'Test Issue Creation Description: ' + self.time
        self.issue_edit_summary = "Test Issue Edit Summary: " + self.time
        self. issue_edit_description = "Test Issue Edit Description: " + self.time

    def test_create_issue(self):
        global issue_key_url
        # login method handles verifying user landed on dashboard page
        login()
        click_link(dashboard_page.create_issue_button)
        sleep(5)
        #Entering Modal.  Begin by asserting Modal id to ensure modal is present.
        assert_element(id='create-issue-dialog')
        write_textfield(issue_modal.summary_field, self.issue_creation_summary)
        write_textfield(issue_modal.description, self.issue_creation_description)
        click_button(issue_modal.submit_issue_button)

        #modal closes. assert notification and assign global key variable the url from notification element.
        wait_for(get_element, css_class='issue-created-key')
        issue_key_url = get_link_url(dashboard_page.get_issue_key_element)

        #assert user lands on issue page
        go_to(issue_key_url)
        wait_for(assert_element , id='footer-comment-button')


    def test_edit_issue(self):
        login()
        #issue page
        go_to(issue_key_url)
        click_link(issue_page.edit_issue_link)
        sleep(5)
        #Entering Edit modal.  Begin by asserting Edit Modal id to ensure modal is present.
        assert_element(id='edit-issue-dialog')
        write_textfield(issue_modal.summary_field, self.issue_edit_summary)
        write_textfield(issue_modal.description, self.issue_edit_description)
        click_element(issue_modal.update_issue_button)
        sleep(5)
        #assert summary on issue page is now "edit summary"
        assert_text(issue_page.issue_summary, self.issue_edit_summary)

    def test_search_issue(self):
        login()
        write_textfield(dashboard_page.quick_search_input_field, self.issue_edit_summary)
        simulate_keys(dashboard_page.quick_search_input_field, 'ENTER')

        # assert the search result page returns the edited issue.
        assert_text(issue_page.issue_summary, self.issue_edit_summary)

